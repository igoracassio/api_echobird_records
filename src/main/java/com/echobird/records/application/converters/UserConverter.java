package com.echobird.records.application.converters;

import com.echobird.records.application.dtos.SignupRequest;
import com.echobird.records.application.models.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class UserConverter {

    @Autowired
    private ModelMapper modelMapper;

    public SignupRequest transformEntityToDto(User user){
        SignupRequest userRequest = new SignupRequest();
        userRequest = modelMapper.map(user, SignupRequest.class);
        return userRequest;
    }

}
